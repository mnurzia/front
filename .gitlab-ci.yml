image: markharding/minds-front-base

services:
  - docker:dind

stages:
  - test:unit
  - build
  - prepare
  - review
  - deploy:staging
  - test:e2e
  - deploy:canary
  - deploy:production

variables:
  CYPRESS_INSTALL_BINARY: 0 # Speeds up the install process
  npm_config_cache: "$CI_PROJECT_DIR/.npm"
  CYPRESS_CACHE_FOLDER: "$CI_PROJECT_DIR/cache/Cypress"

test:
  image: circleci/node:8-browsers
  stage: test:unit
  script:
    - npm ci
    - npm run test -- --no-watch --no-progress --browsers=ChromeHeadlessCI

e2e:base:
  image: cypress/base:10
  stage: test:e2e
  variables:
    CYPRESS_INSTALL_BINARY: 3.4.1
  script:
    - npm ci
    - >
      if [ "$CI_BUILD_REF_NAME" == "master" ]; then
        export E2E_DOMAIN=https://www.minds.com
      else
        export E2E_DOMAIN=https://$CI_BUILD_REF_SLUG.$KUBE_INGRESS_BASE_DOMAIN
      fi
    - export CYPRESS_baseUrl=$E2E_DOMAIN
    - echo "E2E tests for $CI_BUILD_REF_NAME running against $E2E_DOMAIN with user $CYPRESS_username"
    - $(npm bin)/cypress run --record --key $CYPRESS_RECORD_ID --config CYPRESS_baseUrl=$E2E_DOMAIN
  artifacts:
    when: always
    paths:
      - cypress/screenshots
      - cypress/videos
  cache:
    paths:
      - .npm
      - cache/Cypress
  allow_failure: true #manual inspection in case of timeouts

e2e:chrome:
  image: cypress/browsers:chrome67
  stage: test:e2e
  variables:
    CYPRESS_INSTALL_BINARY: 3.4.1
  script:
    - npm ci
    - >
      if [ "$CI_BUILD_REF_NAME" == "master" ]; then
        export E2E_DOMAIN=https://www.minds.com
      else
        export E2E_DOMAIN=https://$CI_BUILD_REF_SLUG.$KUBE_INGRESS_BASE_DOMAIN
      fi
    - export CYPRESS_baseUrl=$E2E_DOMAIN
    - echo "E2E tests for $CI_BUILD_REF_NAME running against $E2E_DOMAIN with user $CYPRESS_username"
    - $(npm bin)/cypress run --browser chrome --record --key $CYPRESS_RECORD_ID --config CYPRESS_baseUrl=$E2E_DOMAIN
  artifacts:
    when: always
    paths:
      - cypress/screenshots
      - cypress/videos
  cache:
    paths:
      - .npm
      - cache/Cypress
  allow_failure: true #manual inspection in case of timeouts

build:review:
  stage: build
  script:
    - npm ci && npm install -g gulp-cli
    - npm run postinstall
    - gulp build.sass && gulp build.sass ##weird build needs to be run twice for now
    - sh build/base-locale.sh dist
  artifacts:
    name: "$CI_COMMIT_REF_SLUG"
    paths:
      - dist
  except:
    refs:
      - master
      - test/gitlab-ci

build:production:en:
  stage: build
  script:
    - npm ci && npm install -g gulp-cli
    - npm run postinstall
    - gulp build.sass --deploy-url=https://cdn-assets.minds.com/front/dist/en && gulp build.sass --deploy-url=https://cdn-assets.minds.com/front/dist/en ##weird build needs to be run twice for now
    - sh build/base-locale.sh dist https://cdn-assets.minds.com/front/dist
  artifacts:
    name: "$CI_COMMIT_REF_SLUG"
    paths:
      - dist/en
  only:
    refs:
      - master
      - test/gitlab-ci

build:production:i18n:
  stage: build
  script:
    - npm ci && npm install -g gulp-cli
    - npm run postinstall
    - gulp build.sass --deploy-url=https://cdn-assets.minds.com/front/dist/en && gulp build.sass --deploy-url=https://cdn-assets.minds.com/front/dist/en ##weird build needs to be run twice for now
    - sh build/i18n-locales-all.sh dist https://cdn-assets.minds.com/front/dist
  artifacts:
    name: "$CI_COMMIT_REF_SLUG"
    paths:
      - dist/vi
  only:
    refs:
      - master
      - test/gitlab-ci

prepare:review:
  stage: prepare
  image: minds/ci:latest
  script:
    - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} 
    - docker build -t $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF -f containers/front-init/Dockerfile dist/.
    - docker push $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF
  dependencies:
    - build:review
  except:
    refs:
      - master
      - test/gitlab-ci

prepare:production:
  stage: prepare
  image: minds/ci:latest
  script:
    - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} 
    - docker build -t $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF -f containers/front-init/Dockerfile dist/.
    - docker push $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF
  only:
    refs:
      - master
      - test/gitlab-ci
  dependencies:
    - build:production:en
    - build:production:i18n

review:start:
  stage: review
  image: minds/helm-eks:latest
  script:
    - aws eks update-kubeconfig --name=sandbox
    - git clone --branch=sandbox-wip https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/minds/helm-charts.git
    - "helm upgrade \
        --install \
        --reuse-values \
        --set frontInit.image.repository=$CI_REGISTRY_IMAGE/front-init \
        --set frontInit.image.tag=$CI_BUILD_REF \
        --set domain=$CI_BUILD_REF_SLUG.$KUBE_INGRESS_BASE_DOMAIN \
        --set elasticsearch.clusterName=$CI_BUILD_REF_SLUG--elasticsearch \
        --wait \
        $CI_BUILD_REF_SLUG \
        ./helm-charts/minds"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_BUILD_REF_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    on_stop: review:stop
  except: 
    refs:
      - master
      - test/gitlab-ci

review:stop:
  stage: review
  image: minds/helm-eks:latest
  script:
    - aws eks update-kubeconfig --name=sandbox
    - helm del --purge $CI_BUILD_REF_SLUG
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: https://$CI_BUILD_REF_SLUG.$KUBE_INGRESS_BASE_DOMAIN
    action: stop
  variables:
    GIT_STRATEGY: none
  when: manual
  except: 
    refs:
      - master
      - test/gitlab-ci

staging:fpm:
  stage: deploy:staging
  image: minds/ci:latest
  script:
    - IMAGE_LABEL="staging"
    ## Sync assets with CDN
    - aws s3 sync dist $S3_REPOSITORY_URL
    - $(aws ecr get-login --no-include-email --region us-east-1)
    ## Update docker front-init container
    - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} 
    - docker pull $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF
    - docker tag $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF $ECR_REPOSITORY_URL:$IMAGE_LABEL
    - docker push $ECR_REPOSITORY_URL:$IMAGE_LABEL
    ## Deploy the new container in rolling restart
    - aws ecs update-service --service=$ECS_APP_STAGING_SERVICE --force-new-deployment --region us-east-1 --cluster=$ECS_CLUSTER
  only:
    refs:
      - master
      - test/gitlab-ci
  dependencies:
    - build:production:en
    - build:production:i18n
  environment:
    name: staging
    url: https://www.minds.com # requires staging cookie

deploy:canary:
  stage: deploy:canary
  image: minds/ci:latest
  script:
    - IMAGE_LABEL="canary"
    ## Sync assets with CDN
    - aws s3 sync dist $S3_REPOSITORY_URL
    - $(aws ecr get-login --no-include-email --region us-east-1)
    ## Update docker front-init container
    - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} 
    - docker pull $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF
    - docker tag $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF $ECR_REPOSITORY_URL:$IMAGE_LABEL
    - docker push $ECR_REPOSITORY_URL:$IMAGE_LABEL
    ## Deploy the new container in rolling restart
    - aws ecs update-service --service=$ECS_APP_CANARY_SERVICE --force-new-deployment --region us-east-1 --cluster=$ECS_CLUSTER
  only:
    refs:
      - master
      - test/gitlab-ci
  dependencies:
    - build:production:en
    - build:production:i18n
  environment:
    name: canary
    url: https://www.minds.com/?canary=1 # requires canary cookie
  when: manual
  allow_failure: false # prevents auto deploy to full production

deploy:production:
  stage: deploy:production
  image: minds/ci:latest
  script:
    - IMAGE_LABEL="production"
    - $(aws ecr get-login --no-include-email --region us-east-1)
    ## Update docker front-init container
    - docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} 
    - docker pull $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF
    - docker tag $CI_REGISTRY_IMAGE/front-init:$CI_BUILD_REF $ECR_REPOSITORY_URL:$IMAGE_LABEL
    - docker push $ECR_REPOSITORY_URL:$IMAGE_LABEL
    ## Deploy the new container in rolling restart
    - aws ecs update-service --service=$ECS_APP_PRODUCTION_SERVICE --force-new-deployment --region us-east-1 --cluster=$ECS_CLUSTER
  only:
    refs:
      - master
      - test/gitlab-ci
  dependencies:
    - build:production:en
    - build:production:i18n
  environment:
    name: production
    url: https://www.minds.com
  when: delayed
  start_in: 2 hours # reduce? can always be deployed manually earlier too
